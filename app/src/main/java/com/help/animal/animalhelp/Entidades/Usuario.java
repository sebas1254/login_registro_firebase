package com.help.animal.animalhelp.Entidades;

public class Usuario {
    private String Correo;
    private String Nombre;

    public Usuario() {
    }

    public String getCorreo() {
        return Correo;
    }

    public void setCorreo(String correo) {
        Correo = correo;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }
}
