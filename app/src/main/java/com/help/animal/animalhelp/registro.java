package com.help.animal.animalhelp;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.DatabaseReference;
import com.help.animal.animalhelp.Entidades.Usuario;

public class registro extends AppCompatActivity {

    private EditText Nombre,correo,contraseña,contraseñaRepetida;
    private Button Registro;
    private FirebaseAuth mAuth;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        Nombre = (EditText) findViewById(R.id.txtNombres);
        correo = (EditText) findViewById(R.id.txtCorreo);
        contraseña = (EditText) findViewById(R.id.txtContraseña);
        contraseñaRepetida = (EditText) findViewById(R.id.txtRcontraseña);
        Registro = (Button) findViewById(R.id.btnRegistrarme);
        mAuth = FirebaseAuth.getInstance();
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference("Usuario");

        Registro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String Correo = correo.getText().toString() ;
                final String nombre = Nombre.getText().toString();
                if (isValidEmail(Correo) && validarContraseña() && validarNombre(nombre)){
                    String Contraseña = contraseña.getText().toString();
                    mAuth.createUserWithEmailAndPassword(Correo, Contraseña)
                            .addOnCompleteListener(registro.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        //el usuario se registro normalmente
                                        Toast.makeText(registro.this, "se registro correctamente",Toast.LENGTH_SHORT).show();
                                        Usuario usuario = new Usuario();
                                        usuario.setCorreo(Correo);
                                        usuario.setNombre(nombre);
                                        databaseReference.push().setValue(usuario);
                                        finish();

                                    } else {
                                        Toast.makeText(registro.this, "Error al registrar", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                }else {
                    Toast.makeText(registro.this,"validacion funcional", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }
 private boolean isValidEmail (CharSequence target){
        return !TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches();
}

public boolean validarContraseña (){
        String Contraseña,ContraseñaRepetida;
        Contraseña = contraseña.getText().toString();
        ContraseñaRepetida = contraseñaRepetida.getText().toString();
        if (Contraseña.equals(ContraseñaRepetida)){
            if (Contraseña.length() >=6){
                return true;
            }else return false;
        }else return false;
}
public boolean validarNombre(String Nombre){
        return !Nombre.isEmpty();
}

}
